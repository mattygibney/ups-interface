# UPS Interface v1.0


-- POST here https://programmingtips17.wordpress.com/2023/09/23/reverse-engineering-a-ups-usb-interface/
## Powercool UPS 650VA - reverse engineering the USB interface to write custom monitoring software 

----
### File descriptions
1. html/template.html - HTML template page containing 6 gauges with {{VAR}} to be substituted by the python code.  
2. monitor.py - Generates a HTML based of a template with the UPS statistics. Copies the file to /var/www/html . Runs every 5 seconds. 
3. services/UPSMonitor.service - allows you to install the program as a service
4. services/WebServer.service - simple service to start the inbuilt python webserver to serve our html page
5. udev/ups-usb.rules - udev rules to allow normal user access to ups use device

### Pre Reqs:
1. install PyUSB
3. clone repo
4. lsusb to find idVendor and idProduct 
5. copy ups-usb.rules to /etc/udev/rules.d/ and update the group for you user. Reboot.

### Running 
1. update monitor.py with you idVendor and idProduct, template and output files
2. sudo python3 monitor.py
3. view the generated html file


### Install

1. Copy UPSMonitor.service to /etc/systemd/system/
2. sudo systemctl start UPSMonitor - start the program 
3. sudo systemctl enable UPSMonitor - enable on boot

1. Copy WebServer.service to /etc/systemd/system/
2. sudo systemctl start WebServer - start the program 
3. sudo systemctl enable WebServer - enable on boot

Alter for the users and directories as needed

![Web Monitor](images/POC-v0.1.png "Web Monitor")

----
### USB descriptor codes found during usb packet sniffing and bruteforce
values that return data:
0 - NONE

1 - MEC0003 - Device name maybe  

3 -  (245.0 000.0 246.0 000 50.0 13.6 29.0 00001000 - UPS data we are looking for 

12 - V3.8 some kind of version 

13 - 12.00 - Battery type 12V 

243 - BL100 - Battery Life maybe 

values that cause the UPS to perform an action:

4 - test the UPS on battery for around 10 seconds

15 - seems to power off the UPS after some time - needs more work 

import usb.core
import usb.util
import time

values = []
templateFilePath="/home/pi/UpsInterface/ups-interface/html/template.html"
outputFilePath="/home/pi/UpsInterface/ups-interface/html/monitor.html"
vid=0x0001
pid=0x0000

#Read template into var
try:
    htmlTemplateFile = open(templateFilePath, "r")
    htmlTemplate = htmlTemplateFile.read()
    htmlTemplateFile.close()
except Exception as e:
    print("Error: Check the template file location")
    print("Current value:", templateFilePath)
    print(e)

#Create a copy we can update
htmlPage = htmlTemplate

while True:
    # find our device based of VID and PID use lsusb to find these
    dev = usb.core.find(idVendor=vid, idProduct=pid)

    # was it found?
    if dev is None:
        raise ValueError('Device not found')
    try:
        #Ask UPS for latest values
        output=usb.util.get_string(dev,3,1)
        #Split on spaces stripping first and last chars
        values=output[1:-1].split(" ")
        #Replace the values in the html template with calculated degrees and fixed value
        htmlPage=htmlPage.replace("{{involtDeg}}", str(int((float(values[0]) / 300) * 180)))
        htmlPage=htmlPage.replace("{{involtVal}}", str(values[0]))
        htmlPage=htmlPage.replace("{{outvoltDeg}}", str(int((float(values[2]) / 300) * 180)))
        htmlPage=htmlPage.replace("{{outvoltVal}}", str(values[2]))
        htmlPage=htmlPage.replace("{{hertzDeg}}", str(int((float(values[4]) / 100) * 180)))
        htmlPage=htmlPage.replace("{{hertzVal}}", str(values[4]))
        htmlPage=htmlPage.replace("{{batvoltDeg}}", str(int((float(values[5]) / 14) * 180)))
        htmlPage=htmlPage.replace("{{batvoltVal}}", str(values[5]))
        htmlPage=htmlPage.replace("{{battempDeg}}", str(int((float(values[6]) / 60) * 180)))
        htmlPage=htmlPage.replace("{{battempVal}}", str(values[6]))
        htmlPage=htmlPage.replace("{{curTime}}", str(time.strftime("%Y-%m-%d %H:%M:%S", time.gmtime())))
        #Check status bit if AC electric has turned off green for good red for bad
        if(str(values[7])[0]=="0"):
            #Green
            htmlPage=htmlPage.replace("{{backgroundColour}}", "0f0") 
        else:
            #Red
            htmlPage=htmlPage.replace("{{backgroundColour}}", "f00") 

        #Write the template to web directory
        text_file1 = open(outputFilePath, "w")
        text_file1.write(htmlPage)
        text_file1.close()
        print("UPS status", values)

    except Exception as e:
        print("Error",e)
    time.sleep(5)
    #Reset html page to replace with new values on next iteration 
    htmlPage=htmlTemplate

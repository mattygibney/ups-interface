import usb.core
import usb.util
import time
import sys

#Add some parameters
# main
delay=sys.argv[1] 
ignoreFile=sys.argv[2] 
#outputFile=sys.argv[3]  
delay=int(delay)

#Load file into array and cast to int 
with open(ignoreFile) as file:
    ignoreValues = [int(line.rstrip()) for line in file]

print(ignoreValues)
#Lets try and test each index from 0 to 255 to see what information comes back 

successList = []
failList = []
testList = []
indexMax=256

for i in range(0,indexMax):
    #Skip these values 
    if( i in ignoreValues):
       continue 
    # find our device use lsusb to get these values 
    dev = usb.core.find(idVendor=0x0001, idProduct=0x0000)

    # quick check  
    if dev is None:
        raise ValueError('Device not found')

    try:
        print(usb.util.get_string(dev,i,1))
    except:
        print("Error")
        failList.append(i)
    else:
        successList.append(i)
    # Lets get the info back from the device as we have found index 3 gives us the UPS stats 
    try:
        time.sleep(delay)
        a=usb.util.get_string(dev,3,1)[1:-1].split(" ")
        print(i,a) 
	# Check if the state has changed have we tripped a test etc 
        if(a[7] != "00001000"):
            testList.append(i)
            print("interesting", i)
    # We might have caused a reset of the UPS hence catch exceptions 
    except:
        print("Error getting values")

    # slowly work through them
    time.sleep(delay)

print("success:", successList)
print("fail:", failList)
print("test:", testList)
